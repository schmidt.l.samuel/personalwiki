"""creates GUI with PyQt, as Main Menu and View"""
import os
from PyQt5 import QtWidgets, uic
import metadata_interface
from sqlite_helper import SqliteHelper


APP = QtWidgets.QApplication([])
DLG = uic.loadUi("db_ui.ui")


def load_notes_data_in_table():
    """Displays notes in table view"""
    helper = SqliteHelper(metadata_interface.get_path_to_notes() + "notes.db")
    note_name = DLG.SearchWidget.text()
    notes = helper.select_notes_by_name(note_name)

    for row_number, note in enumerate(notes):
        DLG.NotesTable.insertRow(row_number)
        for column_number, data in enumerate(note):
            cell = QtWidgets.QTableWidgetItem(str(data))
            DLG.NotesTable.setItem(row_number, column_number, cell)


def clear_table():
    """Clears the table if there is note data stored"""
    while DLG.NotesTable.rowCount() > 0:
        DLG.NotesTable.removeRow(0)




def open_insert_window():
    """executes insert.py"""
    os.system("insert.py")


def open_setting_window():
    """executes settings.py"""
    os.system("settings.py")


def open_edit_window():
    """executes edit.py"""
    helper = SqliteHelper(metadata_interface.get_path_to_notes() + "notes.db")
    note_name = DLG.SearchWidget.text()
    notes = helper.title_to_id(note_name)

    for row_number, note in enumerate(notes):
        os.system("edit.py " + str(note[0]))


def open_learning_window():
    """executes learning_algorithm.py"""
    os.system("learning_algo.py")


DLG.SearchWidget.textChanged.connect(clear_table)
DLG.SearchWidget.textChanged.connect(load_notes_data_in_table)
DLG.EditButton.clicked.connect(open_edit_window)
DLG.InsertButton.clicked.connect(open_insert_window)
DLG.startLearningButton.clicked.connect(open_learning_window)
DLG.actionSettings.triggered.connect(open_setting_window)


DLG.show()
APP.exec()
