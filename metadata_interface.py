"""provides an interface via methods to access metadata stored in metadata.json"""
import json


def get_path_to_notes():
    """Returns path to directory where notes are stored"""
    with open("metadata.json", "r") as json_file:
        json_data = json.load(json_file)
    path_dir = json_data["path_dir"]
    return path_dir


def get_path_to_temp_dir():
    """Returns path to directory where the temporary note is stored"""
    with open("metadata.json", "r") as json_file:
        json_data = json.load(json_file)
    path_dir = json_data["path_dir"] + """temp\\"""
    return path_dir


def set_notes_dir(new_notes_dir):
    """expects a URL where existing notes are stored"""
    with open("metadata.json", "r") as json_file:
        json_data = json.load(json_file)
    json_file.close()

    json_data["path_dir"] = new_notes_dir + "/"

    with open("metadata.json", "w") as json_file:
        json.dump(json_data, json_file)
    json_file.close()