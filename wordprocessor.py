"""provides class to override QTextEdit to allow Drag&Drop, especially from Clipboard"""
import os
import uuid

import metadata_interface

from PyQt5 import QtGui
from PyQt5 import QtWidgets


IMAGE_EXTENSIONS = [".jpg", ".png", ".bmp"]


def hexuuid():
    """creates unique (hexadecimal) id"""
    return uuid.uuid4().hex


def splitext(given_path):
    """returns file extension"""
    return os.path.splitext(given_path)[1].lower()


class TextEdit(QtWidgets.QTextEdit):
    """Overrides QTextEdit wih functions for Drag & Drop"""
    # because they are Qt Function, we cant follow python style
    # pylint: disable=C0103
    note_path = None

    def set_path_to_note_dir(self, note_dir=None):
        if self.note_path is None:
            if note_dir is None:
                self.note_path = metadata_interface.get_path_to_temp_dir()
            else:
                self.note_path = note_dir


    def save_as_html(self, title):
        """saves text from TextEdit as html file to temp dir"""
        self.set_path_to_note_dir()

        path_to_html_file = self.note_path + title + ".html"
        text = self.toHtml()

        with open(path_to_html_file, "w") as file:
            file.write(text)

    def canInsertFromMimeData(self, source):
        """Test if we can insert source with own special method.
        Right now, only for image data. (Not for videos etc.)"""

        if source.hasImage():
            return True
        return super(TextEdit, self).canInsertFromMimeData(source)

    def insertFromMimeData(self, source):
        """Overrides function from QTextEdit to add images per Drag&Drop"""

        cursor = self.textCursor()
        self.set_path_to_note_dir()

        if source.hasUrls():

            for img_url in source.urls():
                file_ext = splitext(str(img_url.toLocalFile()))
                if img_url.isLocalFile() and file_ext in IMAGE_EXTENSIONS:
                    image = QtGui.QImage(img_url.toLocalFile())
                    image_uuid = hexuuid()
                    image.save(self.note_path + str(image_uuid) + ".png")
                    cursor.insertImage(img_url.toLocalFile())

                else:
                    # If we hit a non-image or non-local URL break the loop and fall out
                    # to the super call & let Qt handle it
                    break

            else:
                # If all were valid images, finish here.
                return

        elif source.hasImage():
            image = source.imageData()
            image_uuid = hexuuid()
            image.save(self.note_path + image_uuid + ".png")
            cursor.insertImage(image, str(image_uuid) + ".png")
            return

        super(TextEdit, self).insertFromMimeData(source)
